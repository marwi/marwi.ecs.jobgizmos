using System;
using System.Collections.Generic;
using Unity.Entities;
using UnityEditor;
using UnityEngine;

namespace com.marwi.unity.jobgizmos
{
    public class GizmoSystem : ComponentSystem, IDisposable
    {
        private static readonly List<GizmoBuffer> PendingBuffers = new List<GizmoBuffer>();
        private static bool running = false;

        public GizmoBuffer CreateBuffer()
        {
            var buf = new GizmoBuffer(2048);
            PendingBuffers.Add(buf);
            return buf;
        }

        protected override void OnStartRunning()
        {
            base.OnStartRunning();
            running = true;
        }

        protected override void OnStopRunning()
        {
            Dispose();
            running = false;
        }

        protected override void OnUpdate()
        {
            for (var index = PendingBuffers.Count - 1; index >= 0; index--)
            {
                var buf = PendingBuffers[index];
                if (buf.Age >= 3)
                {
                    buf.Dispose();
                    PendingBuffers.RemoveAt(index);
                }
            }
        }

        public void Dispose()
        {
            foreach (var buf in PendingBuffers)
                buf.Dispose();
            PendingBuffers.Clear();
        }


        [DrawGizmo(GizmoType.Selected | GizmoType.NonSelected)]
        static void DrawGizmos(Transform t, GizmoType type)
        {
            if (!running) return;
            foreach (var buf in PendingBuffers)
            {
                buf.Draw();
                buf.Dispose();
            }

            PendingBuffers.Clear();
        }
    }
}