using System;
using Unity.Mathematics;
using UnityEngine;

namespace com.marwi.unity.jobgizmos
{
    public struct GizmoBuffer : IDisposable
    {
        private DrawableBuffer<LineCommandData> lines;
        private DrawableBuffer<SphereCommandData> spheres;
        private DrawableBuffer<CubeCommandData> cubes;
//        private DrawableBuffer<IconCommandData> icons;
        private readonly int creationFrame;

        public int Age => Time.frameCount - creationFrame;

        internal GizmoBuffer(int size)
        {
            this.creationFrame = Time.frameCount;
            lines = new DrawableBuffer<LineCommandData>(size);
            spheres = new DrawableBuffer<SphereCommandData>(size);
            cubes = new DrawableBuffer<CubeCommandData>(size);
//            icons = new DrawableBuffer<IconCommandData>(size);
        }

        internal void Draw()
        {
            lines.Draw();
            spheres.Draw();
            cubes.Draw();
//            icons.Draw();
        }

        public void Dispose()
        {
            lines.Dispose();
            spheres.Dispose();
            cubes.Dispose();
//            icons.Dispose();
        }

        public void DrawLine(float3 from, float3 to, Color color)
        {
            lines.Add(new LineCommandData()
            {
                @from = from,
                to = to,
                color = color
            });
        }

        public void DrawRay(float3 origin, float3 direction, Color color)
        {
            lines.Add(new LineCommandData()
            {
                @from = origin,
                to = origin+direction,
                color = color
            });
        }

        public void DrawSphere(float3 pos, float radius, Color color)
        {
            spheres.Add(new SphereCommandData()
            {
                position = pos,
                radius = radius,
                color = color,
                solid = true,
            });
        }

        public void DrawWireSphere(float3 pos, float radius, Color color)
        {
            spheres.Add(new SphereCommandData()
            {
                position = pos,
                radius = radius,
                color = color,
                solid = false,
            });
        }

        public void DrawCube(float3 pos, float3 scale, Color color)
        {
            cubes.Add(new CubeCommandData()
            {
                position = pos,
                scale = scale,
                color = color,
                solid = true
            });
        }

        public void DrawWireCube(float3 pos, float3 scale, Color color)
        {
            cubes.Add(new CubeCommandData()
            {
                position = pos,
                scale = scale,
                color = color,
                solid = false
            });
        }

//        public void DrawIcon(float3 center, string name, bool allowScaling = false)
//        {
//            icons.Add(new IconCommandData()
//            {
//                center = center,
//                name = name,
//                allowScaling = allowScaling
//            });
//        }
    }

    public struct LineCommandData : IDrawable
    {
        public float3 from;
        public float3 to;
        public Color color;

        public void Draw()
        {
            Gizmos.color = color;
            Gizmos.DrawLine(@from, to);
        }
    }

    public struct SphereCommandData : IDrawable
    {
        public float3 position;
        public float radius;
        public Color color;
        public bool solid;

        public void Draw()
        {
            Gizmos.color = color;
            if (solid)
                Gizmos.DrawSphere(position, radius);
            else
                Gizmos.DrawWireSphere(position, radius);
        }
    }

    public struct CubeCommandData : IDrawable
    {
        public float3 position;
        public float3 scale;
        public Color color;
        public bool solid;

        public void Draw()
        {
            Gizmos.color = color;
            if (solid)
                Gizmos.DrawCube(position, scale);
            else
                Gizmos.DrawWireCube(position, scale);
        }
    }

    public struct IconCommandData : IDrawable
    {
        public float3 center;
        public string name;
        public bool allowScaling;

        public void Draw()
        {
            Gizmos.DrawIcon(center, name, allowScaling);
        }
    }
}