﻿using System;
using Unity.Collections;

namespace com.marwi.unity.jobgizmos
{
    public struct DrawableBuffer<T> : IDisposable, IDrawable where T : struct, IDrawable
    {
        [NativeDisableParallelForRestriction] 
        private NativeArray<T> data;
        private int index;


        internal DrawableBuffer(int size, Allocator alloc = Allocator.TempJob)
        {
            this.data = new NativeArray<T>(size, alloc);
            this.index = -1;
        }

        internal void Add(T command)
        {
            if (index >= data.Length)
                return;
            ++index;
            data[index] = command;
        }

        internal bool IsCreated => data.IsCreated;

        public void Draw()
        {
            if (!data.IsCreated) return;
            for (var i = 0; i < data.Length && index < i; i++)
            {
                var d = data[i];
                d.Draw();
            }
        }

        public void Dispose()
        {
            if (data.IsCreated)
                data.Dispose();
        }
    }
    
 
}