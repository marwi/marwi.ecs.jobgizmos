namespace com.marwi.unity.jobgizmos
{
    public interface IDrawable
    {
        void Draw();
    }
}