﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace com.marwi.unity.jobgizmos._TestComponents
{
    public class GridGizmoSpawner : MonoBehaviour, IConvertGameObjectToEntity
    {
        public int X = 10;
        public int Z = 10;
    
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponent<SpawnData>(entity);
            dstManager.SetComponentData<SpawnData>(entity, new SpawnData()
            {
                countX = X,
                countZ = Z,
            });
        }
    }

    public class DummyGizmoSystem : JobComponentSystem
    {
        private struct MyJob : IJobForEach<Translation>
        {
            public float time;

            public GizmoBuffer JobGizmo;

            public void Execute(ref Translation c0)
            {
                c0.Value.y += math.sin(time) * 0.1f;
                JobGizmo.DrawLine(c0.Value, c0.Value + new float3(0,1,0)  * c0.Value.y * 3f, Color.red);
                JobGizmo.DrawWireSphere(c0.Value, 1f, Color.blue);
                JobGizmo.DrawWireCube(c0.Value, c0.Value.yyy * .5f, Color.magenta);
//                JobGizmo.DrawIcon(c0.Value + new float3(0,1,0), "TEST 123");
            }
        }

        private EndSimulationEntityCommandBufferSystem commands;
        private GizmoSystem gizmoSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            this.commands = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            this.gizmoSystem = World.GetOrCreateSystem<GizmoSystem>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var handle = new MyJob()
            {
                time = Time.time,
                JobGizmo = this.gizmoSystem.CreateBuffer(),
            }.Schedule(this, inputDeps);
            this.commands.AddJobHandleForProducer(handle);

            return handle;
        }
    }


    struct SpawnData : IComponentData
    {
        public int countX;
        public int countZ;
    }

    public class GizmosSpawnerSystem : JobComponentSystem
    {
        struct GizmoSpawnJob : IJobForEachWithEntity<SpawnData>
        {
            public EntityCommandBuffer.Concurrent Buffer;


            public void Execute(Entity entity, int index, ref SpawnData data)
            {
                for (var x = 0; x < data.countX; x++)
                {
                    for(var z = 0; z < data.countZ; z++)
                    {
                        var instance = Buffer.CreateEntity(index);
                        Buffer.AddComponent(index, instance, new Translation()
                        {
                            Value = new float3(x, 0, z)
                        });
                    }
                }

                Buffer.DestroyEntity(index, entity);
            }
        }

        private EndSimulationEntityCommandBufferSystem sys;

        protected override void OnCreate()
        {
            base.OnCreate();
            sys = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var handle = new GizmoSpawnJob()
            {
                Buffer = sys.CreateCommandBuffer().ToConcurrent(),
            }.Schedule(this, inputDeps);
            sys.AddJobHandleForProducer(handle);
            return handle;
        }
    }
}